# RestEngine

[![CI Status](http://img.shields.io/travis/Roberto Previdi/RestEngine.svg?style=flat)](https://travis-ci.org/Roberto Previdi/RestEngine)
[![Version](https://img.shields.io/cocoapods/v/RestEngine.svg?style=flat)](http://cocoapods.org/pods/RestEngine)
[![License](https://img.shields.io/cocoapods/l/RestEngine.svg?style=flat)](http://cocoapods.org/pods/RestEngine)
[![Platform](https://img.shields.io/cocoapods/p/RestEngine.svg?style=flat)](http://cocoapods.org/pods/RestEngine)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RestEngine is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RestEngine"
```

## Author

Roberto Previdi, hariseldon78@gmail.com

## License

RestEngine is available under the MIT license. See the LICENSE file for more info.
